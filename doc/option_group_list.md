### Option Group List

### Request

```
PUT /option_groups/action

{
    "list":{
        "marker": 1,
        "max_records": 20,
    }
}
```


### Response

```
{
    "option_groups": [
        {
            "option_group": {
                    "id": "2342342-2342342-2342424",
                    "name": "demo-option-group-0",
                    "description": "this is a test option group",
                    "state": "avaiable"
             }        
        },
        {
            "option_group":{
                    "id": "2342342-2342342-2342424",
                    "name": "demo-option-group-1",
                    "description": "this is a test option group",
                    "state": "avaiable"
            }
        }
    ]
}
```


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```
