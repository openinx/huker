## Parameter Group Change

### Request

```
PUT /parameter_groups/action

{
    "change":{
        "parameter_group_id": "32342222222-23432342-23423",
        "parameters":[
            {
                "parameter": {
                    "name": "max_user_connections",
                    "value": "24",
                    "apply_method": "pending-reboot"
                }
            },
            {
                "parameter": {
                    "name": "max_allowed_packet",
                    "value": "1024",
                    "apply_method": "immediate"
                }
            }
        ]
    }
}
```


* apply_method: 
   * pending-reboot:
      等待实例重启之后才能生效
   * immediate: 
      修改完成之后立即生效， 这只使用于mysql的static类型参数。


### Response

```
{
    "request_id": "2342-23423423-2342342-2342342",
}
```



### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```
