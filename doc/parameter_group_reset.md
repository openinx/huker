## Parameter Group Reset To Default Value 

### Request

```
PUT /parameter_groups/action

{
    "reset":{
        "parameter_group_id": "32342222222-23432342-23423",
        "reset_all_paremeters": 0,
        "parameters":[
            {
                "parameter": {
                    "name": "max_user_connections",
                    "apply_method": "pending-reboot"
                }
            },
            {
                "parameter": {
                    "name": "max_allowed_packet",
                    "apply_method": "immediate"
                }
            }
        ]
    }
}

OR 


{
    "reset":{
        "parameter_group_id": "32342222222-23432342-23423",
        "reset_all_paremeters": 1,
    }
}
```


### Response

No Response


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```
