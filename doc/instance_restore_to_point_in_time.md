## Instance Restore To Point In Time

### Request-1

```
POST  /instances/action
{
    "restore_to_point_in_time": {
        "restore_time": "2013-12-16 00:00:00",
        "instance_type": "mini",
        "name": "the-name-of-new-instance",
        "description": "this is a instance resotre from snapshot",
        "structure": "single-master",
        "user": "openinx",
        "password": "openinx-password",
        "datastore_type": "mysql",
        "datastore_version": "5.5",
        "option_group_id": "2342322-sdfsdfsdf-2342ksdfoj22342",
        "parameter_group_id": "0898u2342-23420sfk234023sdj0dsf-32",
        "security_group_id": "2kjku-2342psixks0w23-23423sdd",
        "avail_zone": "subnet46",
    }
}
```

### Request-2

```
POST /instance/action
{
    "restore_to_point_in_time": {
        "to_lastest_restorable_time": "false",
        "instance_type": "mini",
        "name": "the-name-of-new-instance",
        "description": "this is a instance resotre from snapshot",
        "structure": "single-master",
        "user": "openinx",
        "password": "openinx-password",
        "datastore_type": "mysql",
        "datastore_version": "5.5",
        "option_group_id": "2342322-sdfsdfsdf-2342ksdfoj22342",
        "parameter_group_id": "0898u2342-23420sfk234023sdj0dsf-32",
        "security_group_id": "2kjku-2342psixks0w23-23423sdd",
        "avail_zone": "subnet46",
    }
}
```


### Response

```
{
    "instance":{
        "id": "2342342-2342424-23424242",
        "name": "demo-instance",
        "description": "this is a test instance",
        "state": "creating",
        "state_info": "",
        "address": "192.168.194.33",
        "port": 3306,
        "security_group_id": "70a599e0-31e7-49b7-b260-868f441e862b",
        "parameter_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23",
        "option_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23"
        "instance_type": "mini",
        "structure": "single-master",
        "user": "test-mysql-user",
        "password": "test-mysql-user-password",
        "datastore_type": "mysql",
        "datastore_version": "5.5",
        "avail_zone": "subnet46",
    }
}
```

No Response




###ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

