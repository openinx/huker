##Instance List

###Request

```
GET /instances/action

{
    "list": {
        "marker": 1,
        "max_records": 20,
    }
}
```

* marker : 非负数, 分页的页号
* max_records: 正数 ， 返回的记录的条数 

###Response

```
{
    "instances": [
        {
            "instance":{
                "id": "2342342-2342424-23424242",
                "name": "demo-instance",
                "description": "this is a test instance",
                "state": "creating",
                "state_info": "",
                "endpoints": [
                    {
                        "address": "192.168.194.33",
                        "port": 3306,
                    }
                ]
                "security_group_id": "70a599e0-31e7-49b7-b260-868f441e862b",
                "parameter_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23",
                "option_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23"
                "flavor": "mini",
                "structure": "single-master",
                "user": "test-mysql-user",
                "datastore_type": "5.5",
                "datastore_version": "5.5",
                "avail_zone": "subnet46",
            }
        }    
    ]
}
```


###ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

