## Promotes a read replica DB instance to a DB instance.


+ preferred_backup_window

The daily time range during which automated backups are created if automated backups are enabled, using the BackupRetentionPeriod parameter.
Default: A 30-minute window selected at random from an 8-hour block of time per region. See the Amazon RDS User Guide for the time blocks for each region from which the default backup windows are assigned.
Constraints: Must be in the format hh24:mi-hh24:mi. Times should be Universal Time Coordinated (UTC). Must not conflict with the preferred maintenance window. Must be at least 30 minutes.
Type: String
Required: No

### Request

```
POST  /instances/action
{
    "promote_read_replica": {
        "instance_id": "2342342-2342424-23424242",
        "flavor":  "mini",
        "preferred_backup_window": "00:00:00-00:30:00",
    }
}
```

### Response

```
{
    "instance":{
        "id": "2342342-2342424-23424242",
        "name": "demo-instance",
        "description": "this is a test instance",
        "state": "creating",
        "state_info": "",
        "endpoints": [
            {  
                "address": "192.168.194.33",
                "port": 3306,
                "type": "read_write",
            }, 
            {
                "address": "192.168.194.32",
                "port": 3306,
                "type": "read_only",
            },
        ],
        "security_group_id": "70a599e0-31e7-49b7-b260-868f441e862b",
        "parameter_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23",
        "option_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23"
        "instance_type": "mini",
        "structure": "single-master",
        "user": "test-mysql-user",
        "password": "test-mysql-user-password",
        "datastore_type": "mysql5.5",
        "datastore_version": "5.5",
    }
}
```

No Response




###ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

