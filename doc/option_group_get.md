### Option Group Get

###Request

```
GET /option_group/{option_group_id}
```

### Response


```
{
    "option_group": {
        "id": "2342342-2342342-2342424",
        "name": "demo-option-group",
        "description": "this is a test option group",
        "state": "available",
        "options": [
            { 
                "option": {
                    "name": "binlog_flush_peroid",
                    "data_type": "integer",
                    "value": 300,
                    "apply_method": "pending-reboot"
                }
            },
            {
                "option": {
                    "name": "binlog_expire_time",
                    "data_type": "integer",
                    "value": 604800,
                    "apply_method": "immediate"
                }
            },
            {
                "option": {
                    "name": "snapshot_create_period",
                    "data_type": "integer",
                    "value": 604800,
                    "apply_method": "immediate"
                }
            },
            {
                "option": {
                    "name": "snapshot_expire_time",
                    "data_type": "integer",
                    "value": 604800,
                    "apply_method": "immediate"
                }
            },
            {
                "option": {
                    "name": "backup_type",
                    "data_type": "string",
                    "value": "mysqldump|xtrabackup",
                    "apply_method": "immediate"
                }
            }
       ] 
    }
}
```


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

