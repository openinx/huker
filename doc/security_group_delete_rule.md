## Security Group Delete Rule

### Request

```
PUT /security_groups/action
{
    "delete_security_group_rules": {
        "security_group_id": "helloworld-iamauuid4-23424",
        "allowed_hosts": [
            "192.168.194.0/24",
            "172.17.255.0/24",
        ]
    }
}
```

### Response

```
{
    "security_group":{
        "id": "2342342-2342424-23424242",
        "name": "demo-security-group",
        "description": "this is a test security group",
        "state": "avaiable",
        "allowed_hosts": [],
    }
}
```


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

