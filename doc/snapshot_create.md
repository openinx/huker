## Snapshot Create

### Request

```
POST /snapshots
{
    "snapshot":{
        "name": "demo-snapshot-name",
        "instance_id": "sdfsdfffffffffffs234-23423423242-23",
        "backup_type": "xtrabackup" | "mysqldump",
    }
}
```

* backup_type: 目前支持两种备份方式, 全部都用gzip压缩的方式
   * xtrabackup
   * mysqldump


### Response

```
{
    "snapshot": {
        "size_bytes": 2342342,
        "compressed_size_bytes": 100000,
        "checksum": "66286db64c1749a58945a61849c7719b",
        "compressed_checksum": "66286db64c1749a58945a61849c7719b",
        "instance_id": "sdfsdfffffffffffs234-23423423242-23",
        "name": "demo-snapshot-name",
        "datastore_type": "mysql",
        "datastore_version": "5.5",
        "type": "auto" ,
        "status": "creating",  
        "backup_time": "2013-12-18 00:00:00.001",
        "backup_cost_seconds": 60,
    }
}
```
* size_bytes: 镜像未压缩之前的字节数,
* compressed_size_bytes: 镜像压缩之后的的字节数，
* checksum: 镜像压缩之前的md5sum, 
* compressed_checksum: 镜像压缩之后的md5sum. 
* type:  有两种选择
    * auto : 手动创建镜像
    * manual: 系统自动创建镜像
* status:  有以下几种状态
    * creating
    * error
    * available
* backup_time: 镜像开始备份的时间点
* backup_cost_seconds: 备份过程中花费的时间，单位为seconds.
 
### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

