## Parameter Group List

### Request

```
PUT /parameter_groups/action

{
    "list":{
        "parameter_group_id": "sdfsehlwloworld-2333223dsdss",
        "marker": 1,
        "max_records": 20,
    }
}
```


### Response

No Response



### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```
