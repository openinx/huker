## Instance Create

### Request

```
POST /instances
{
    "instance":{
        "name": "demo-instance",
        "description": "this is a test instance",
        "security_group_id": "70a599e0-31e7-49b7-b260-868f441e862b",
        "parameter_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23",
        "option_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23",
        "flavor": "mini",
        "structure": "single-master",
        "user": "test-mysql-user",
        "password": "test-mysql-user-password",
        "datastore_type": "mysql",
        "datastore_version": "5.5",
        "avail_zone": "subnet46",
    }
}
```

* name:  只能包含 `[a-zA-Z0-9]` 和 `-` 字符，不能出现两个连着的`-`的字符，首字符为字母, 末尾不能是 `-`, 例如 `aaaa-2342-2342-2-342`
* description:  不能超过255个字符。 
* security_group_id: 必须存在的安全组对应的UUID 
* parameter_group_id:  必须存在的参数组对应的UUID
* flavor: 从给定的实例行号中选择。 现在提供 mini, macro, large, xlarge 四种型号。 
   
   * mini
   * macro
   * large
   * xlarge

* structure: 现在提供两种选择： 
   
   *  single-master: 
      单一的一个主节点，会产生binlog , 做镜像。 但是没有复制节点。 
   *  master-slave: 
      一个主节点， 和一个从节点。 从节点采用semi-sync的方式和主节点复制。 

* user:  只能包含`[a-zA-Z0-9]`和 `-` 字符，不能出现两个连着的`-`的字符，首字符为字母, 末尾不能是 `-`, 长度在5~40个字符之间。 管里整个实例的管理员帐号。 所有的外网IP都可以连接上去。
* password: `[a-zA-Z0-9]`， 长度在15-40个字符之间。数据库密码。 
* datastore_type: 数据库类型。现在提供三种： 

   *  mysql
   *  mariadb 
   *  mongodb

* datastore_version: 
   
   * 若datastore_type为mysql, 可选择： 
       *  5.5
   * 若datastore_type为mariadb， 可选择： 
       *  5.10
   * 若datastore_type为mongodb , 可以选择： 
       *  2.4 

* avail_zone: 


### Response

##### Header

```
HTTP/1.1 202 Accepted
Date: Mon, 23 Jul 2012 20:24:48 GMT
Content-Type: application/json
Request-Id: req-ab05045a-452f-4b46-be0d-86494da02a2b
```

#### BODY

```
{
    "instance":{
        "id": "2342342-2342424-23424242",
        "name": "demo-instance",
        "description": "this is a test instance",
        "state": "creating",
        "state_info": "",
        "endpoints": [
            {
                "address": "192.168.194.33",
                "port": 3306,
                "type": "read_only",
            }
        ],
        "security_group_id": "70a599e0-31e7-49b7-b260-868f441e862b",
        "parameter_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23",
        "option_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23"
        "flavor": "mini",
        "structure": "single-master",
        "user": "test-mysql-user",
        "datastore_type": "mysql",
        "datastore_version": "5.5",
        "avail_zone": "subnet46",
    }
}
```


### ERROR

* 400  InvalidInput :  参数非法
* 404  NotFound:  该API不存在
* 404  SecurityGroupNotFound
* 404  ParameterGroupNotFound
* 404  OptionGroupNotFound
* 413  OverLimit 资源超过配额
* 500  InternalError： 内部错误

