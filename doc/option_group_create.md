### Option Group Create

### Request

```
POST /option_groups
{
    "option_groups":{
        "name": "demo-parameter-groups",
        "description": "this is a test paremeter group",
    }
}
```

* name:  只能包含 `[a-zA-Z0-9]` 和 `-` 字符，不能出现两个连着的`-`的字符，首字符为字母, 末尾不能是 `-`, 例如 `aaaa-2342-2342-2-342`
* description:  不能超过255个字符。 

### Response


```
{
    "option_group":{
        "id": "2342342-2342424-23424242",
        "name": "demo-security-group",
        "description": "this is a test security group",
        "options": [],
    }
}
```


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

