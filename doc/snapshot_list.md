## Snapshot List

### Request

```
GET /snapshots/action

{
    "list": {
        "marker": 1,
        "max_records": 20,
    }
}
```

### Response

```
{
    "snapshots": [
        {
            "snapshot": {
                "size_bytes": 2342342,
                "compressed_size_bytes": 100000,
                "checksum": "66286db64c1749a58945a61849c7719b",
                "compressed_checksum": "66286db64c1749a58945a61849c7719b",
                "instance_id": "sdfsdfffffffffffs234-23423423242-23",
                "name": "demo-snapshot-name",
                "datastore_type": "mysql",
                "datastore_version": "5.5",
                "type": "auto" ,
                "status": "creating",  
                "backup_time": "2013-12-18 00:00:00.001",
                "backup_cost_seconds": 60,
            }
        }
    ]
}


```


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

