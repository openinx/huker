## Instance Restore from Snapshot

### Request

```
POST  /instances/action
{
    "restore_from_snapshot": {
        "snapshot_id": "23423-2342342-23423423-234234",
        "flavor": "mini",
        "name": "the-name-of-new-instance",
        "description": "this is a instance resotre from snapshot",
        "structure": "single-master",
        "user": "openinx",
        "password": "openinxPassword",
        "datastore_type": "mysql",
        "datastore_version": "5.5",
        "option_group_id": "2342322-sdfsdfsdf-2342ksdfoj22342",
        "parameter_group_id": "0898u2342-23420sfk234023sdj0dsf-32", 
        "security_group_id": "2kjku-2342psixks0w23-23423sdd",
    }
}
```

* parameter_group_id:  must be  the same as the base snapshot.



### Response

```
{
    "instance":{
        "id": "2342342-2342424-23424242",
        "name": "demo-instance",
        "description": "this is a test instance",
        "state": "creating",
        "state_info": "",
        "endpoints": [
            {
                "address": "192.168.194.33",
                "port": 3306,
            }
        ]
        "security_group_id": "70a599e0-31e7-49b7-b260-868f441e862b",
        "parameter_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23",
        "option_group_id": "sdfsdfsfsf-sdfsfs2333222-234-23432-23"
        "flavor": "mini",
        "structure": "single-master",
        "user": "test-mysql-user",
        "datastore_type": "mysql",
        "datastore_version": "5.5",
        "avail_zone": "subnet46",
    }
}
```

No Response




### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

