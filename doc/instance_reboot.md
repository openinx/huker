##Instance Reboot

##Request

```
PUT /instances/action
{
    "reboot": {
        "instance_id": "234234-2342323-23423423-234234",
    }
}
```


###Response

No Response




###ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

