## Option Group Change

### Request

```
PUT /option_group/action

{
    "change":{
        "option_group_id": "32342222222-23432342-23423",
        "options":[
            {
                "option": {
                    "name": "binlog_flush_peroid",
                    "value": 300,
                }
            },
            {
                "option": {
                    "name": "binlog_keep_time",
                    "value": 604800,
                }
            },
            {
                "option": {
                    "name": "snapshot_create_period",
                    "value": 604800,
                }
            },
            {
                "option": {
                    "name": "snapshot_keep_time",
                    "value": 604800,
                }
            },
            {
                "option": {
                    "name": "backup_prefered_window",
                    "value":"00:00:00-1:00:00",
                }
            }
        ]
    }
}
```


* binlog_flush_peroid:  定时上传BINLOG的时间间隔
* binlog_expire_time: 定时删除云存储BINLOG的时间间隔
* snapshot_create_period:  定时制作备份镜像的时间间隔
* snapshot_expire_time:  定时删除备份镜像的时间间隔
* backup_prefered_window: 选择备份的时间窗口, 形式为"HH:MM:SS-HH:MM:SS" , 系统会在这个时间段内开始备份


### Response

No Response


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```
