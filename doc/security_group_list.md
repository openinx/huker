## Security Group List

### Request

```
GET /security_groups/action
{
    "list":{
        "marker": 1,
        "records": 20,
    }
}
```

### Response

```
{
    "security_groups":[
        {
            "security_group": [
                "id": "2342342-2342424-23424242",
                "name": "demo-security-group",
                "description": "this is a test security group",
                "state": "creating",
                "allowed_hosts": [],
            ],
        },
        {
            "security_group": [
                "id": "2342342-2342424-23424242",
                "name": "demo-security-group",
                "description": "this is a test security group",
                "state": "creating",
                "allowed_hosts": [],
            ],
        }
    ]
}
```


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

