### Parameter Group Get Default

### Request

```
GET /parameter_groups/action
{
    "get_default":{
        "datastore_type": "mysql",
        "datastore_version": "5.5.32",
    }
}
```

### Response

```
{
    "parameters":[
        {
            "parameter": {
                "data_type": "boolean",
                "is_modifiable": 0,
                "name": "allow-suspicious-udfs",
                "apply_type": "static",
                "allowed_values": "0,1",
            }
        },
        {
            "parameter": {
                "data_type": "integer",
                "is_modifiable": 0,
                "name": "auto_increment_increment",
                "apply_type": "dynamic",
                "allowed_values": "1-65535",
            }
        },
        {
            "parameter": {
                "data_type": "integer",
                "is_modifiable": 0,
                "name": "auto_increment_offset",
                "apply_type": "dynamic",
                "allowed_values": "1-65535",
            }
        }
    ]
}
```


### ERROR

```
400 BadRequest                  // Parameter Error
401 Forbidden                   // authenticate failed
405 BadMethod                   // not supported api
413 OverLimit                   // resource over limit
404 NotFound                    // resource not found
500 InternalError               // Internal Server Error
```

