import os
import struct


class NotExpectedEventError(Exception):
    pass


class NotMySQLBinlogFileError(Exception):
    pass


class MySQLBinlogPaserError(Exception):
    pass


def to_uint8(byts):
    retval, = struct.unpack('B', byts)
    return retval


def to_uint16(byts):
    retval, = struct.unpack('H', byts)
    return retval


def to_uint32(byts):
    retval, = struct.unpack('I', byts)
    return retval


def to_uint64(byts):
    retval, = struct.unpack('Q', byts)
    return retval


def to_char(byts):
    retval, = struct.unpack('c', byts)
    return retval
