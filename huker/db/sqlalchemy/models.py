'''
Created on Nov 23, 2013

@author: openinx
'''

from sqlalchemy import Column, Index, Integer, String
from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey, Boolean, Text
from sqlalchemy.orm import relationship

from huker.openstack.common.db.sqlalchemy import models

BASE = declarative_base()



def MediumText():
    return Text().with_variant(MEDIUMTEXT(), 'mysql')


class HukerBase(models.SoftDeleteMixin,
               models.TimestampMixin,
               models.ModelBase):
    metadata = None


class Datastore(BASE, HukerBase):
    __tablename__ = 'datastore'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255), nullable=False)
    version = Column(String(255), nullable=False)


class Instance(BASE, HukerBase):
    __tablename__ = 'instance'
    __table_args__ = (
        Index('instance_uuid_idx', 'uuid', unique=True),
    )

    id = Column(Integer, primary_key=True, autoincrement=True)
    uuid = Column(String(36), nullable=False)

    user_id = Column(String(255))
    project_id = Column(String(255))

    security_group_id = Column(Integer, ForeignKey('security_group.id'))
    parameter_group_id = Column(Integer, ForeignKey('parameter_group.id'))
    option_group_id = Column(Integer, ForeignKey('option_group.id'))

    name = Column(String(255), nullable=False)
    description = Column(MediumText(), default='')

    state = Column(String(255))
    state_info = Column(String(255))

    address = Column(String(255))
    port = Column(Integer)

    datastore_id = Column(Integer, ForeignKey('datastore.id'))
    db_flavor_id = Column(Integer, ForeignKey('db_flavor.id'))

    user = Column(String(255), nullable=False)
    password = Column(String(255), nullable=False)

    # single-master, master-slave
    structure = Column(String(255), nullable=False)

    zone_id = Column(Integer, ForeignKey('zone.id'))

    # followers are relationship
    security_group = relationship('SecurityGroup', foreign_keys=security_group_id)
    parameter_group = relationship('ParameterGroup', foreign_keys=parameter_group_id)
    option_group = relationship('OptionGroup', foreign_keys=option_group_id)
    datastore = relationship('Datastore', foreign_keys=datastore_id)
    db_flavor = relationship('DBFlavor', foreign_keys=db_flavor_id)
    zone = relationship('Zone', foreign_keys=zone_id)


class Zone(BASE, HukerBase):
    __tablename__ = 'zone'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    description = Column(MediumText(), default='', nullable=False)


class Service(BASE, HukerBase):
    """Represents a running service on a host."""

    __tablename__ = 'services'
    id = Column(Integer, primary_key=True, autoincrement=True)
    host = Column(String(255))  # , ForeignKey('hosts.id'))
    binary = Column(String(255))
    topic = Column(String(255))
    report_count = Column(Integer, nullable=False, default=0)
    disabled = Column(Boolean, default=False)
    availability_zone = Column(String(255), default='huker')
    disabled_reason = Column(String(255))


class OSFlavor(BASE, HukerBase):
    __tablename__ = 'os_flavor'
    id = Column(Integer, primary_key=True, autoincrement=True)

    cpu_cores = Column(Integer, nullable=False)
    mem_bytes = Column(Integer, nullable=False)
    swap_bytes = Column(Integer, default=0, nullable=False)
    disk_bytes_avail = Column(Integer, nullable=False)
    system_version = Column(String(50), nullable=False)
    description = Column(MediumText(), default='')

    disk_rbps = Column(Integer)  # read bytes per second
    disk_wbps = Column(Integer)  # write bytes per second

    network_brandwidth = Column(Integer)


class DBFlavor(BASE, HukerBase):
    __tablename__ = 'db_flavor'
    id = Column(Integer, primary_key=True, autoincrement=True)

    cpu_cores = Column(Integer, nullable=False)
    mem_bytes = Column(Integer, nullable=False)
    swap_bytes = Column(Integer, default=0, nullable=False)
    disk_bytes_avail = Column(Integer, nullable=False)

    max_datadir_bytes = Column(Integer, nullable=False)
    max_logdir_bytes = Column(Integer, nullable=False)

    disk_rbps = Column(Integer)  # read bytes per second
    disk_wbps = Column(Integer)  # write bytes per second

    network_brandwidth = Column(Integer)



class DBServer(BASE, HukerBase):
    __tablename__ = 'server'
    __table_args__ = (
        Index('server_private_ip_idx',
              'private_ip', 'deleted',
              unique=True),
    )
    id = Column(Integer, primary_key=True, autoincrement=True)

    user_id = Column(String(255))
    project_id = Column(String(255))

    instance_id = Column(Integer, ForeignKey('instance.id'))
    db_flavor_id = Column(Integer, ForeignKey('db_flavor.id'))

    private_ip = Column(String(255))
    public_ip = Column(String(255))
    # virtual,  baremetal, lxc, docker
    server_type = Column(String(255), nullable=True)

    role = Column(String(36))  # master, slave, unknown, S-ReadOnly,

    state = Column(String(40))

    # relationship
    instance = relationship('Instance', foreign_keys=instance_id)
    db_flavor = relationship('DBFlavor', foreign_keys=db_flavor_id)


class LinuxServer(BASE, HukerBase):
    __tablename__ = 'linux_server'
    id = Column(Integer, primary_key=True, autoincrement=True)

    os_flavor_id = Column(Integer, ForeignKey('os_flavor.id'))

    private_ip = Column(String(255))
    public_ip = Column(String(255), nullable=True)

    state = Column(String(40))
    zone_id = Column(Integer, ForeignKey('zone.id'))

    # relationship
    os_flavor = relationship('OSFlavor', foreign_keys=os_flavor_id)
    zone = relationship('Zone', foreign_keys=zone_id)

class VIP(BASE, HukerBase):
    __tablename__ = 'vip'
    id = Column(Integer, primary_key=True, autoincrement=True)

    address = Column(String(255))
    state = Column(String(40))
    zone_id = Column(Integer, ForeignKey('zone.id'))

    # relationship
    zone = relationship('Zone', foreign_keys=zone_id)


class ParameterGroup(BASE, HukerBase):
    __tablename__ = 'parameter_group'
    __table_args__ = (
        Index('parameter_group_uuid_idx', 'uuid', unique=True),
    )
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    uuid = Column(String(36), nullable=False)

    user_id = Column(String(255))
    project_id = Column(String(255))

    name = Column(String(255), nullable=False)
    description = Column(MediumText(), default='')


class Parameter(BASE, HukerBase):
    __tablename__ = 'parameter'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    parameter_group_id = Column(Integer, ForeignKey('parameter_group.id'))
    name = Column(String(255), nullable=False)

    # int, str, bool, choice
    data_type = Column(String(255), nullable=False)
    value = Column(String(255), nullable=True)

    # immediate or pending-reboot
    apply_method = Column(String(255))
    is_modifiable = Column(Boolean, default=False)
    allowed_values = Column(String(255))

    # version information
    current_version = Column(Integer, default=0)
    change_version = Column(Integer, default=0)

    # relationship
    parameter_group = relationship('ParameterGroup', foreign_keys=parameter_group_id)


class SecurityGroup(BASE, HukerBase):
    __tablename__ = 'security_group'

    __table_args__ = (
        Index('security_group_uuid_idx', 'uuid', unique=True),
    )

    id = Column(Integer, primary_key=True, autoincrement=True)
    uuid = Column(String(36), nullable=False)
    name = Column(String(255), nullable=False)

    description = Column(MediumText(), default='')


    user_id = Column(String(255))
    project_id = Column(String(255))


class SecurityGroupRule(BASE, HukerBase):
    __tablename__ = 'security_group_rule'
    id = Column(Integer, primary_key=True, autoincrement=True)
    security_group_id = Column(Integer, ForeignKey('security_group.id'))

    cidr = Column(String(255), nullable=True)
    from_port = Column(Integer, nullable=True, default=2000)
    to_port = Column(Integer, nullable=True, default=65535)

    security_group = relationship('SecurityGroup', foreign_keys=security_group_id)



class SystemIptableRule(BASE, HukerBase):
    __tablename__ = 'system_iptable_rule'

    id = Column(Integer, primary_key=True, autoincrement=True)
    protocol = Column(String(5), default='tcp')  # 'tcp', 'udp', 'icmp'

    src_from_port = Column(Integer)
    src_to_port = Column(Integer)

    dst_from_port = Column(Integer)
    dst_to_port = Column(Integer)

    cidr = Column(String(255))


class CloudStorageObj(BASE, HukerBase):  # Cloud Storage Object.
    __tablename__ = 'cloud_storage_obj'

    object_name = Column(String(120), primary_key=True)
    bucket_name = Column(String(120))

    state = Column(String(10))  # uploading, error, uploaded.

    compressed = Column(Boolean, default=False)
    compressed_algorithm = Column(String(20), default='gzip')  # gzip, lzo, lz4,

    size_bytes = Column(Integer)
    compressed_size_bytes = Column(Integer)

    checksum = Column(String(40))
    compressed_checksum = Column(String(40))

    # seconds to alive,
    # If object exceed time_alive, system will delete the object.
    # when alive_secs = -1 , then the object will be alive forever.
    alive_secs = Column(Integer)

    # time token to upload the object to cloud storage.
    upload_time_secs = Column(Integer)


class Snapshot(BASE, HukerBase):
    __tablename__ = 'snapshot'

    __table_args__ = (
        Index('snapshot_uuid_idx', 'uuid', unique=True),
    )

    id = Column(Integer, primary_key=True, autoincrement=True)
    uuid = Column(String(36))

    user_id = Column(String(255))
    project_id = Column(String(255))

    name = Column(String(255), nullable=False)
    instance_id = Column(Integer,
                         ForeignKey('instance.id'),
                         nullable=False)

    datastore_id = Column(Integer, ForeignKey('datastore.id'))

    cloud_storage_obj_name = Column(String(255),
                                    ForeignKey('cloud_storage_obj.object_name'))

    create_type = Column(String(50))  # auto, manual
    backup_type = Column(String(255))  # MySQL/Mariadb: xtrabackup/mysqldump

    state = Column(String(255), nullable=False)

    # relationship
    cs_obj = relationship('CloudStorageObj', foreign_keys=CloudStorageObj.object_name)


class DBLogFile(BASE, HukerBase):

    __tablename__ = 'db_log_file'

    id = Column(Integer, primary_key=True, autoincrement=True)

    instance_id = Column(Integer, ForeignKey('instance.id'))

    name = Column(String(255))

    start_timestamp = Column(Integer)
    end_timestamp = Column(Integer)

    start_pos = Column(Integer)
    end_pos = Column(Integer)

    state = Column(String(255))

    cloud_storage_obj = Column(String(255),
                               ForeignKey('cloud_storage_obj.object_name'))

    # relationship
    cs_obj = relationship('CloudStorageObj', foreign_keys=CloudStorageObj.object_name)



class OptionGroup(BASE, HukerBase):
    __tablename__ = 'option_group'
    __table_args__ = (
       Index('option_group_uuid_idx', 'uuid', unique=True),
    )

    id = Column(Integer, primary_key=True, autoincrement=True)
    uuid = Column(String(36))

    user_id = Column(String(255))
    project_id = Column(String(255))

    uuid = Column(String(255))
    name = Column(String(255), nullable=False)

    description = Column(MediumText(), default='')
    state = Column(String(255))


class Option(BASE, HukerBase):
    __tablename__ = 'option'
    id = Column(Integer, primary_key=True, autoincrement=True)
    option_group_id = Column(Integer, ForeignKey('option_group.id'))
    name = Column(String(255), nullable=False)
    data_type = Column(String(255), nullable=False)
    value = Column(String(255))
    apply_method = Column(String(255))

    # relationship
    option_group = relationship('OptionGroup', foreign_keys=OptionGroup.id)


class Quota(BASE, HukerBase):
    """Represents the current usage for a given resource."""

    __tablename__ = 'quota'
    id = Column(Integer, primary_key=True, autoincrement=True)

    project_id = Column(String(255), index=True)
    resource = Column(String(255))

    in_use = Column(Integer)
    hard_limit = Column(Integer)
