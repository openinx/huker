'''
Created on May 15, 2014

@author: hz
'''

from datetime import datetime

from huker.db.sqlalchemy import models as mod
from huker.openstack.common.db.sqlalchemy.session import get_session

def now(): return datetime.now()

cfg = {
    # datastore static
    'datastore': [
        {'name': 'mysql', 'version': '5.5.32'},
        {'name': 'mysql', 'version': '5.6.30'},
        {'name': 'mariadb', 'version': '5.5.10'}
    ],

    # static parameter
    'default_parameter_group': { 'name': 'default', 'description': 'default' },
    
    # static quota
    'default_quota': [
        {'resource': 'instance', 'in_use': 0, 'hard_limit': 10},
        {'resource': 'linux_server', 'in_use': 0, 'hard_limit': 10},
        {'resource': 'option_group', 'in_use': 0, 'hard_limit': 10},
        {'resource': 'security_group', 'in_use': 0, 'hard_limit': 10},
        {'resource': 'snapshot', 'in_use': 0, 'hard_limit': 10},
        {'resource': 'parameter_group', 'in_use': 0, 'hard_limit': 10},
   ],

    # default security group
    'default_security_group': {
        'name': 'default',
        'description': 'default',
        'rule': [
            {'cidr': '0.0.0.0/0', 'from_port': 2000, 'to_port': 65535}
        ],
    },
    # default zone
    'zones': [
        {
            'name': 'zone1',
            'description': 'zone1',
        },
        {
            'name': 'zone2',
            'description': 'zone2'
        },
    ]
}

MYSQL_PARAMETERS = [
    #basedir
    {'name': 'basedir',
     'data_type': 'str',
     'value': '/database/app',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #datadir
    {'name': 'datadir',
     'data_type': 'str',
     'value': '/database/data',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #tmpdir
    {'name': 'tmpdir',
     'data_type': 'str',
     'value': '/database/data/tmpdir',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #max_connections
    {'name': 'max_connections',
     'data_type': 'int',
     'value': '1024',
     'apply_method': 'pending-reboot',
     'is_modifiable': 1,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #innodb_log_file_size
    {'name': 'innodb_log_file_size',
     'data_type': 'int',
     'value': str(512*1024*1024),
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #innodb_log_files_in_group
     {'name': 'innodb_log_files_in_group',
     'data_type': 'int',
     'value': str(2),
     'apply_method': 'pending-reboot',
     'is_modifiable': 1,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #innodb_flush_log_at_trx_commit
    {'name': 'innodb_flush_log_at_trx_commit',
     'data_type': 'int',
     'value': str(1),
     'apply_method': 'pending-reboot',
     'is_modifiable': 1,
     'allowed_values': '0|1|2',
     'current_version': 0,
     'change_version':0},     
    #innodb_file_per_table
    {'name': 'innodb_file_per_table',
     'data_type': 'int',
     'value': str(1),
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},     
    #innodb_buffer_pool_size
    {'name': 'innodb_buffer_pool_size',
     'data_type': 'int',
     'value': str(512*1024*1024),
     'apply_method': 'pending-reboot',
     'is_modifiable': 1,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #innodb_flush_method
    {'name': 'innodb_flush_method',
     'data_type': 'str',
     'value': 'O_DIRECT',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #character_set_server
     {'name': 'character_set_server',
     'data_type': 'str',
     'value': 'utf8',
     'apply_method': 'pending-reboot',
     'is_modifiable': 1,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #collation_server
    {'name': 'collation_server',
     'data_type': 'str',
     'value': 'utf8_general_ci',
     'apply_method': 'pending-reboot',
     'is_modifiable': 1,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #binlog_format
     {'name': 'binlog_format',
     'data_type': 'str',
     'value': 'ROW',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #user
     {'name': 'user',
     'data_type': 'str',
     'value': 'database',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #sync_binlog
     {'name': 'sync_binlog',
     'data_type': 'int',
     'value': str(1),
     'apply_method': 'pending-reboot',
     'is_modifiable': 1,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #log_bin
     {'name': 'log_bin',
     'data_type': 'str',
     'value': '/database/log/master_bin',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #log_bin_index
     {'name': 'log_bin_index',
     'data_type': 'str',
     'value': '/database/log/master_bin.index',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #log_slave_updates
    {'name': 'log_slave_updates',
     'data_type': 'bool',
     'value': 'TRUE',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #pid_file
     {'name': 'pid_file',
     'data_type': 'str',
     'value': '/database/mysqld.pid',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #socket
    {'name': 'socket',
     'data_type': 'str',
     'value': '/database/mysql.sock',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #port
    {'name': 'port',
     'data_type': 'int',
     'value': '3306',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #bind_address
    {'name': 'bind_address',
     'data_type': 'str',
     'value': '0.0.0.0',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #log_error
    {'name': 'log_error',
     'data_type': 'str',
     'value': '/database/log/error.log',
     'apply_method': 'pending-reboot',
     'is_modifiable': 0,
     'allowed_values': None,
     'current_version': 0,
     'change_version':0},
    #expire_logs_days
     {'name': 'expire_logs_days',
      'data_type': 'int',
      'value': '7',
      'apply_method': 'pending-reboot',
      'is_modifiable': 0,
      'allowed_values': None,
      'current_version': 0,
      'change_version':0},
]


def import_default_parameter_group(ctxt):
    for d in cfg['default_parameter_group']:
        
        
        
        
        

def import_datastore():
    for d in cfg['datastore']:
        ds = mod.Datastore()
        ds.update(d)
        ds.save()


def import_db_flavor():
    pass


def import_default_quota(ctxt):
    for d in cfg['default_quota']:
        quota = mod.Quota()
        quota.update(d)
        quota.project_id = ctxt.project_id
        quota.save()

def import_zone():
    for d in cfg['default_zone']:
        zone = mod.Zone()
        zone.update(d)
        zone.save()


def import_default_security_group(ctxt):
    session = get_session()
    with session.begin():
        d = cfg['default_security_group']
        sg = mod.SecurityGroup()
        sg.name = d['name']
        sg.description = d['description']
        sg.user_id = ctxt.user_id
        sg.project_id = ctxt.project_id
        sg.save(session=session)

        for dr in d['rule']:
            sg_rule = mod.SecurityGroupRule()
            sg_rule.update(dr)
            sg_rule.security_group_id = sg.id
            sg_rule.save(session=session)

def sync_to_database():
    pass
