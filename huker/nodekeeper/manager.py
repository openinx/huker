'''
Created on Nov 23, 2013

@author: openinx
'''
"""
NodeKeeper Manager
"""

import time
from huker.openstack import manager


class NodekeeperManager(manager.Manager):
    
    RPC_API_VERSION = '1.0'

    def __init__(self, *args, **kwargs):
        super(NodekeeperManager, self).__init__(service_name='nodekeeper',
                                           *args, **kwargs)

    def create_instance(self):
        pass


    def delete_instance(self):
        pass

    def create_security_group(self):
        pass

    def delete_security_group(self):
        pass
    
    def test(self, ctxt, arg):
        print 'argument:%s' % arg
        time.sleep(10)
        return 'argument:%s' % arg
