'''
Created on Nov 23, 2013

@author: openinx
'''

from oslo.config import cfg
from huker.openstack.common.rpc import proxy as rpc_proxy

CONF = cfg.CONF


class NodekeeperAPI(rpc_proxy.RpcProxy):
    
    BASE_RPC_API_VERSION = '1.0'
    VERSION_CAP ='1.0'
    
    def __init__(self, topic=None):
        topic = topic if topic else CONF.nodekeeper_topic
        version_cap = self.VERSION_CAP
        super(NodekeeperAPI, self).__init__(
	            topic=topic,
	            default_version=self.BASE_RPC_API_VERSION,
	            version_cap=version_cap)
    
    def test(self, ctxt, arg):
        return self.call(ctxt, 
		                 self.make_msg(method='test',
                                       arg=arg),
                         timeout=12)
