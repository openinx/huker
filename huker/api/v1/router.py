"""
WSGI middleware for OpenStack Volume API.
"""

import huker.api.openstack
from huker.api.v1 import instances
from huker.openstack.common import log as logging


LOG = logging.getLogger(__name__)


class APIRouter(huker.api.openstack.APIRouter):
    """Routes requests on the API to the appropriate controller and method."""

    def _setup_routes(self, mapper):

        self.resources['instance'] = instances.create_resource()

        mapper.connect("instance",
                       "/instance/show/{instance_id}",
                       controller=self.resources['instance'],
                       action='show',
                       conditions={"method": ['GET']})
