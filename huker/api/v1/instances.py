'''
Created on Apr 29, 2014

@author: hz
'''

from huker.api.openstack import wsgi
from huker.api.views import instances as views_instances
from huker.api.views import instances as instances_views
import huker.db.api as db


class InstancesController(wsgi.Controller):

    _view_builder_class = views_instances.ViewBuiler

    def show(self, req, instance_id):
        ctxt = req.environ['huker.context']
        instance = db.instance_get(ctxt, instance_id)
        return self._view_builder.show(req, instance)


    def _get_view_builder(self, req):
        return instances_views.ViewBuiler()


def create_resource():
    return wsgi.Resource(InstancesController())
