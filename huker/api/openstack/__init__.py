"""
WSGI middleware for OpenStack API controllers.
"""

import routes

from huker.api.openstack import wsgi
from huker.openstack.common import log as logging
from huker import wsgi as base_wsgi


LOG = logging.getLogger(__name__)


class APIMapper(routes.Mapper):
    def routematch(self, url=None, environ=None):
        if url is "":
            result = self._match("", environ)
            return result[0], result[1]
        return routes.Mapper.routematch(self, url, environ)


class APIRouter(base_wsgi.Router):
    """Routes requests on the API to the appropriate controller and method."""

    @classmethod
    def factory(cls, global_config, **local_config):
        """Simple paste factory, :class:`huker.wsgi.Router` doesn't have."""
        return cls()

    def __init__(self):
        mapper = APIMapper()
        self.resources = {}
        self._setup_routes(mapper)
        super(APIRouter, self).__init__(mapper)

    def _setup_routes(self, mapper):
        raise NotImplementedError
